### Developed Personal Portfolio Website Using HTML, CSS, and JavaScript ###
### This portfolio website has an about section that shows my description as a web developer ###
### a skills section with progress bars showing how much I know about different programming languages ###
### a projects section that shows my projects and certifications 
### a blog section that shows information about my experiences with technology development and agile workflow ###
### a timeline section showing my different projects 
### a contact section that includes a form to input user information###
